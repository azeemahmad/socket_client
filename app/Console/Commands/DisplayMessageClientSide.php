<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Events\PublicMessageSent;
use App\DisplayNumber;
use App\HomeMessage;



class DisplayMessageClientSide extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DisplayMessageClientSide:DisplayMessageClientSide';

    protected $description = 'Display Message Client Side';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        $displayMessage=DisplayNumber::pluck('message_id');
        $messageHome=HomeMessage::whereNotIn('id',$displayMessage)->get();
        if(isset($messageHome) && $messageHome->isNotEmpty()){
            foreach ($messageHome as $key => $value){
                $data['message_id'] = $value->id;
                DisplayNumber::create($data);
                $disMessgae=$value->message;
                $this->displayMessage($disMessgae);
                //sleep for 2 seconds
                usleep(2000000);
            }
        }

    }
    public function displayMessage($message){
        PublicMessageSent::dispatch($message);
        return response(request('message'), 200);
    }

}
